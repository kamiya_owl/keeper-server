package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/codegangsta/negroni"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"

	valid "github.com/asaskevich/govalidator"

	"github.com/globalsign/mgo"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Email    string        `bson:"email"`
	PassHash string        `bson:"passHash"`
	Name     string        `bson:"name"`
}
type Post struct {
	ID   bson.ObjectId `bson:"_id,omitempty"`
	By   string        `bson:"by"` // userId
	Data string        `bson:"data"`
	At   time.Time     `bson:"at"`
}

// passwordhash生成、検証用
func generateHash(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
func comparePassAndHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func getAuthUserID(r *http.Request, users *mgo.Collection) (bson.ObjectId, error) {
	user := r.Context().Value("user")
	claims := user.(*jwt.Token).Claims.(jwt.MapClaims)
	userID := claims["userId"]
	if userID == nil {
		return "", fmt.Errorf("userID is missing")
	}
	id := bson.ObjectIdHex(userID.(string))
	return id, nil
}
func returnBson(w http.ResponseWriter, data interface{}) {
	j, _ := bson.MarshalJSON(data)
	w.Header().Set("Content-Type", "application/json, charset=utf-8")
	fmt.Fprintf(w, string(j))
}

func main() {
	// TODO: Config ENVに移管
	mongodbConnectionString := "mongodb://localhost:27017/keeper"
	secretKey := "TODO:MOVE_TO_ENV"
	passLengthMin := 6
	passLengthMax := 32
	nameLengthMin := 1
	nameLengthMax := 32
	signMethod := jwt.SigningMethodHS256
	// mongodb
	mongo, err := mgo.DialWithTimeout(mongodbConnectionString, 20*time.Second)
	if err != nil {
		panic(err)
	}
	// TODO: Error handling
	defer mongo.Close()
	db := mongo.DB("keeper")
	users := db.C("user")
	posts := db.C("post")

	// jwtの初期設定
	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		},
		SigningMethod: signMethod,
	})
	// routerの設定
	publicRouter := mux.NewRouter().StrictSlash(true)
	privateRouter := mux.NewRouter().StrictSlash(true)
	// 非認証API
	publicRouter.HandleFunc("/api/1.0/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "it works.")
	})
	publicRouter.HandleFunc("/api/1.0/register", func(w http.ResponseWriter, r *http.Request) {
		// bodyをjsonパースする
		b, err0 := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err0 != nil {
			http.Error(w, err0.Error(), http.StatusBadRequest)
			return
		}
		var data map[string]string
		err0 = json.Unmarshal(b, &data)
		if err0 != nil {
			http.Error(w, err0.Error(), http.StatusBadRequest)
			return
		}
		email := data["email"]
		pass := data["pass"]
		name := data["name"]
		if !valid.IsEmail(email) {
			http.Error(w, "emailが正しくありません", http.StatusBadRequest)
			return
		}
		if !valid.IsByteLength(pass, passLengthMin, passLengthMax) {
			http.Error(w, "パスワードが6文字から32文字の長さに収まっている必要があります", http.StatusBadRequest)
			return
		}
		if !valid.IsByteLength(name, nameLengthMin, nameLengthMax) {
			http.Error(w, "名前が入力されていないか32文字を超えています", http.StatusBadRequest)
			return
		}
		// すでに登録済のメアドか確認
		u := bson.M{}
		err1 := users.Find(bson.M{"email": email}).One(&u)
		if u["_id"] != nil || (err1 != nil && err1.Error() != "not found") {
			http.Error(w, "既に登録済のアカウントです", http.StatusBadRequest)
			return
		}
		// 登録
		passHash, _ := generateHash(pass)
		err2 := users.Insert(User{
			Name: name, PassHash: passHash, Email: email,
		})
		if err2 != nil {
			http.Error(w, err2.Error(), http.StatusInternalServerError)
			return
		}
		// jsonで返してあげる
		err3 := users.Find(bson.M{"email": email}).Select(bson.M{"passHash": 0}).One(&u)
		if err3 != nil {
			http.Error(w, err3.Error(), http.StatusInternalServerError)
			return
		}
		returnBson(w, u)
	})
	publicRouter.HandleFunc("/api/1.0/authenticate", func(w http.ResponseWriter, r *http.Request) {
		// bodyをjsonパースする
		b, err0 := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err0 != nil {
			http.Error(w, err0.Error(), http.StatusBadRequest)
			return
		}
		var data map[string]string
		err0 = json.Unmarshal(b, &data)
		if err0 != nil {
			http.Error(w, err0.Error(), http.StatusBadRequest)
			return
		}
		email := data["email"]
		pass := data["pass"]
		if !valid.IsEmail(email) {
			http.Error(w, "emailが正しくありません", http.StatusBadRequest)
			return
		}
		// すでに登録済のメアドか確認
		u := User{}
		err1 := users.Find(bson.M{"email": email}).One(&u)
		if err1 != nil {
			http.Error(w, "指定されたメールアドレスは登録されていません", http.StatusBadRequest)
			return
		}
		// ハッシュ比較
		if !comparePassAndHash(pass, u.PassHash) {
			http.Error(w, "パスワードが一致しません", http.StatusBadRequest)
			return
		}
		token := jwt.NewWithClaims(signMethod, jwt.MapClaims{
			"userId": u.ID,
		})
		tokenStr, err2 := token.SignedString([]byte(secretKey))
		if err2 != nil {
			http.Error(w, err2.Error(), http.StatusInternalServerError)
			return
		}
		// passHash入り込むのめんどいのでここで作って返す
		returnBson(w, bson.M{
			"token": tokenStr,
			"user":  bson.M{"_id": u.ID.Hex(), "email": u.Email, "name": u.Name}})
	})
	// 認証つきAPI
	privateRouter.HandleFunc("/api/1.0/post", func(w http.ResponseWriter, r *http.Request) {
		id, err := getAuthUserID(r, users)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		idStr := id.Hex()
		switch r.Method {
		case "GET":
			var results []Post
			posts.Find(bson.M{"by": idStr}).All(&results)
			returnBson(w, results)
		case "POST":
			b, err0 := ioutil.ReadAll(r.Body)
			defer r.Body.Close()
			if err0 != nil {
				http.Error(w, err0.Error(), http.StatusBadRequest)
				return
			}
			var data map[string]string
			err0 = json.Unmarshal(b, &data)
			if err0 != nil {
				http.Error(w, err0.Error(), http.StatusBadRequest)
				return
			}
			d := data["data"]
			// 作る
			post := Post{
				Data: d, By: idStr, At: time.Now(),
			}
			err1 := posts.Insert(post)
			if err1 != nil {
				http.Error(w, err1.Error(), http.StatusBadRequest)
				return
			}
			returnBson(w, post)
		default:
			http.Error(w, "Not Implemented", http.StatusNotImplemented)
			return
		}
	}).Methods("GET", "POST")

	// TODO: Impl here
	// privateRouter.HandleFunc("/api/1.0/post/{post_id}", func(w http.ResponseWriter, r *http.Request) {
	//	get/update/delete
	// }).Methods("GET", "PUT", "DELETE")

	// privateRouterにjwt認証をつけて合体
	secured := negroni.New(negroni.HandlerFunc(jwtMiddleware.HandlerWithNext), negroni.Wrap(privateRouter))
	publicRouter.PathPrefix("/api").Handler(secured)
	// たちあげる
	n := negroni.Classic()
	n.UseHandler(publicRouter)
	n.Run(":8080")
}
